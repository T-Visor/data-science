/* Note: All coordinates are in the form of [latitude, longitude].
         This script references the Leaflet Javascript library (which should be initialized in index.html) */

/**
 * Bootstraps the process for displaying Baltimore, Maryland
 * on the map 
 */
function run() {
    var centerOfBaltimore = [39.2902778, -76.6125]; // TODO: find a way to query these coordinates instead of hardcoding them
    var mapnikBlackAndWhite = "http://{s}.tiles.wmflabs.org/bw-mapnik/{z}/{x}/{y}.png"; // url for the map provider
    var borderStyle = {
        "color": "black",
        "weight": 5,
        "opacity": 0.65
    };

    // Get the geojson data for Baltimore, Maryland as a responseJSON object
    // from the Open Baltimore website.
    // (Note: we must use a url with ajax because it uses an HTTP GET request)
    var baltimoreBoundary = $.ajax({
        url:"https://data.baltimorecity.gov/api/geospatial/fy7v-tvcr?method=export&format=GeoJSON",
        dataType: "json"
    })

    // Wait for the ajax query to get the geojson data before we start building the map.
    // This ensures the rest of the code will execute properly.
    $.when(baltimoreBoundary).done(function() {
        var map = createMap(centerOfBaltimore);
        setTileLayer(map, mapnikBlackAndWhite);
        drawOutline(map, borderStyle, baltimoreBoundary);
        drawRouteColors(map);
    });
}
run();

//=======================================================================================

/**
 * Generate a new map object with dimensions specified by div "mapDimensions" in index.html
 * and center it around Baltimore city.
 * 
 * @param centerCoordinates [latitude, longitude] the center of some location on a map
 *                          (could be a country, city, town, etc)
 * 
 * @returns a map object
 */
function createMap(centerCoordinates) {
    var mapConfigurations = {
        center: centerCoordinates,
        zoom: 11
    }

    return new L.map("mapDimensions", mapConfigurations);
}

/**
 * Mainly for aesthetics, this will give a particular look
 * to the map depending on which map provider is used for rendering
 * 
 * @param map the map object
 * @param mapProvider a string url for which map provider to use
 */
function setTileLayer(map, mapProvider) {
    var layer = new L.TileLayer(mapProvider);
    map.addLayer(layer);
}

/**
 * Draws an outline representing a location on a map
 * 
 * @param map the map object
 * @param drawingStyle the list of attributes which will represent the style for drawing the outline
 * @param locationBoundary the responseJSON object containing a list of coordinates 
 *                         representing a boundary for some location (state, country, city are some examples)
 */
function drawOutline(map, drawingStyle, locationBoundary) {
    L.geoJSON(locationBoundary.responseJSON, {
        style: drawingStyle
    }).addTo(map);
}

/**
 * This function is only for demonstration purposes. It will simply
 * draw some colored lines somewhere within Baltimore City.
 * 
 * @param map the map object 
 */
function drawRouteColors(map) {
    // add a green line to the map
    var greenRoute = [[39.2994, -76.6388], [39.3494, -76.5638]];
    var firstLine = L.polyline(greenRoute, {
        color: "green",
        weight: 5,
        opacity: 100
    });
    firstLine.addTo(map);

    // add a yellow line to the map
    var yellowRoute = [[39.3577,-76.6753,], [39.3601,-76.5584]];
    var secondLine = L.polyline(yellowRoute, {
        color: "yellow",
        weight: 5,
        opacity: 100
    });
    secondLine.addTo(map);

    // add a red line to the map
    var redRoute = [[39.2404, -76.5993], [39.2657,-76.5452]];
    var thirdLine = L.polyline(redRoute, {
        color: "red",
        weight: 5,
        opacity: 100
    });
    thirdLine.addTo(map);
}